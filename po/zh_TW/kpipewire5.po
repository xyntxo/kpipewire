# Chinese translations for kpipewire package
# kpipewire 套件的正體中文翻譯.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the kpipewire package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kpipewire\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-31 00:52+0000\n"
"PO-Revision-Date: 2022-05-31 00:52+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: pipewirecore.cpp:86
#, kde-format
msgid "Failed to create PipeWire context"
msgstr ""

#: pipewirecore.cpp:96
#, kde-format
msgid "Failed to connect to PipeWire"
msgstr ""

#: pipewirecore.cpp:103
#, kde-format
msgid "Failed to start main PipeWire loop"
msgstr ""
